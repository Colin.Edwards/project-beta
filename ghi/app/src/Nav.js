import { NavLink } from 'react-router-dom';
function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Sales</a>
              <div className='dropdown-menu'>
                <NavLink className="dropdown-item" to="/salespersons/new/">New Sales Person</NavLink>
                <NavLink className="dropdown-item" to="/sales/new/">New Sales Record</NavLink>
                <NavLink className="dropdown-item" to="/salespersons/">Sales Person History</NavLink>
                <NavLink className="dropdown-item" to="/sales/">Sales Records</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Service</a>
              <div className='dropdown-menu'>
                <NavLink className="dropdown-item" to="/service/new/">New Service</NavLink>
                <NavLink className="dropdown-item" to="/service/">Service List</NavLink>
                <NavLink className="dropdown-item" to="/service/history/">Service History</NavLink>
                <NavLink className="dropdown-item" to="/technician/new/">New Technician</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Manucfacturers</a>
              <div className='dropdown-menu'>
                <NavLink className="dropdown-item" to="/manufacturers/">Manufacturers</NavLink>
                <NavLink className="dropdown-item" to="/manufacturer/new/">New Manufacturer</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Automobiles</a>
              <div className='dropdown-menu'>
                <NavLink className="dropdown-item" to="/automobiles/">List Automobile</NavLink>
                <NavLink className="dropdown-item" to="/automobiles/new/">New Automobile</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Customers</a>
              <div className='dropdown-menu'>
                <NavLink className="dropdown-item" to="/customers/new/">New Customer</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Models</a>
              <div className='dropdown-menu'>
                <NavLink className="dropdown-item" to="/models/new/">New Model</NavLink>
                <NavLink className="dropdown-item" to="/models/">Model List</NavLink>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
