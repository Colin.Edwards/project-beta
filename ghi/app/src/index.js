import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadManufactures(){
  const response = await fetch('http://localhost:8100/api/manufacturers/');
  if (response.ok) {
    const data = await response.json()
    root.render(
      <React.StrictMode>
        <App manufaturers={data.manufacturers}/>
      </React.StrictMode>
    )
  }
}