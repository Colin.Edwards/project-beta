import React from 'react'

class CustomerForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: "",
            address: "",
            phone: "",
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleAddressChange = this.handleAddressChange.bind(this)
        this.handleNumberChange = this.handleNumberChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleNameChange(event){
        const value = event.target.value;
        this.setState({name: value})
    }
    handleAddressChange(event){
        const value = event.target.value;
        this.setState({address: value})
    }
    handleNumberChange(event){
        const value = event.target.value;
        this.setState({phone: value})
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: "",
                address: "",
                phone: "",
            };
            this.setState(cleared);
        };
    };

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Customer</h1>
                        <form onSubmit={this.handleSubmit} id="new-customer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleAddressChange} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNumberChange} value={this.state.phone} placeholder="Phone Number" required type="text" name="phone" id="phone" className="form-control"/>
                                <label htmlFor="phone">Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default CustomerForm;