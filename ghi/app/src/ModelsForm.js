import React from 'react';

class ModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            pictureUrl: "",
            manufacturer: "",
            manufacturers: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.picture_url = data.pictureUrl
        data.manufacturer_id = data.manufacturer
        delete data.pictureUrl;
        delete data.manufacturers;
        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(modelUrl, fetchConfig);

        if (response.ok) {
            const cleared = {
                name: "",
                pictureUrl: "",
                manufacturer: "",
            };
            this.setState(cleared);
            window.location.reload();
        };
    };

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({pictureUrl: value})
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers})
        }
    }

    render() {
        return (
            <div className='row'>
                <div className='shadow p-4 mt-4'>
                    <h1>New Vehicle</h1>
                    <form onSubmit={this.handleSubmit} id="create-model-form">
                        <div className='form-floating mb-3'>
                            <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className='form-control'/>
                            <label htmlFor='name'>Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={this.handlePictureChange} value={this.state.pictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className='form-control'/>
                            <label htmlFor='picture_url'>Picture Link</label>
                        </div>
                        <div className='mb-3'>
                            <select onChange={this.handleManufacturerChange} value={this.state.manufacturer} required id="manufacturer" name="manufacturer" className='form-select'>
                                <option value="">Select Manufacturer</option>
                                {this.state.manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className='btn btn-primary' id="vehicleModelBtn">Create</button>
                    </form>
                </div>
            </div>
        )
    }

}

export default ModelForm