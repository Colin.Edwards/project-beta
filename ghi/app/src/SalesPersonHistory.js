import React from "react";

class SalesPersonHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      salespeople: [],
      salesrecords: [],
    };
    this.handleSalesPeople = this.handleSalesPeople.bind(this);
    this.handleSaleRecord = this.handleSaleRecord.bind(this)
  }
  handleSalesPeople(event){
    const value = event.target.value
    this.setState({salesperson: value})
  }

  async handleSalesPerson(event){
    const url = 'http://localhost:8090/api/salespersons'
    const response = await fetch(url)
    if(response.ok){
      const data = await response.json();
      this.setState({salespeople: data.salesperson})
    }
  }
  async handleSaleRecord(event){
    const url = 'http://localhost:8090/api/sales'
    const response = await fetch(url)
    try {
      if(response.ok){
        const data = await response.json();
        this.setState({salesrecords: data.sales_records})
      }
    } catch (e) {
      console.error(e);
    }
  }
  componentDidMount(){
    this.handleSaleRecord();
    this.handleSalesPerson();
  }

  render() {
    return (
      <>
        <div className="mb-3">
          <select onChange={this.handleSalesPeople} required id="salesperson" name="salesperson" className="form-select"  >
            <option value="">Select a Salesperson</option>
            {this.state.salespeople.map(sale => {
              return (
                <option key={sale.id} value={sale.id}>
                  {sale.name}
                </option>
              );
            })}
          </select>
        </div>
        <table className='table table-striped'>
            <thead>
              <tr>
                <th>Sales Person</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Sales Price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.salesrecords.filter(
                sale => sale.salesperson.id.toString() === this.state.salesperson).map(sale =>{
                return(
                  <tr key={sale.id}>
                    <td>{sale.salesperson.name}</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>${sale.price}</td>
                  </tr>
                )
              })}
            </tbody>
        </table>
      </>
    );
  }
}
export default SalesPersonHistory;
