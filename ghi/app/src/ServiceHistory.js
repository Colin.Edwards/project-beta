import React from 'react';

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            services: []
        }
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const historyUrl = `http://localhost:8080/api/service/history/${this.state.vin}/`
        const request = await fetch(historyUrl);
        if (request.ok) {
            const data = await request.json();
            this.setState({ services: data.services });
        }
    }

    render() {
        return (
            <div className='row'>
                <div className='offset-1 col-10'>
                    <div className='p-4 m-4'>
                        <form className='input-group mb-3'>
                            <input onChange={this.handleVinChange} value={this.state.vin} placeholder="Enter Vehicle VIN" required type="text" name={this.state.vin} id={this.state.vin} className="form-control"/>
                            <button onClick={this.handleSubmit} type='submit' className='btn btn-primary input-group-append'>Search Service History</button>
                        </form>
                        <h1>Service History</h1>
                        <table className='table table-striped table-hover'>
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th>Owners</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Reason</th>
                                    <th>Technician</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.services.map(service => {
                                    return (
                                        <tr key={service.id}>
                                            <td>{service.vin}</td>
                                            <td>{service.owner}</td>
                                            <td>{service.date}</td>
                                            <td>{service.time}</td>
                                            <td>{service.reason}</td>
                                            <td>{service.technician.name}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceHistory