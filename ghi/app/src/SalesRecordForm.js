import React from 'react'

class SalesRecord extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            salespeople: [],
            customers: [],
            autos: [],
            price: "",
            customer_name: "",
            automobile_name: "",
            sales_person: "",
        }
        this.handleSalesperson = this.handleSalesperson.bind(this)
        this.handleAutomobile = this.handleAutomobile.bind(this)
        this.handleCustomer = this.handleCustomer.bind(this)
        this.handlePrice = this.handlePrice.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {
            automobile: this.state.automobile_name,
            salesperson: this.state.sales_person,
            price: this.state.price,
            customer: this.state.customer_name,
        }
        const url = `http://localhost:8090/api/sales/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const cleared = {
                price: "",
                customer_name: '',
                automobile_name: "",
                sales_person: "",
            }
            this.setState(cleared)
        }
    };
    handleSalesperson(event){
        const value = event.target.value;
        this.setState({sales_person: value})
    }
    handleAutomobile(event){
        const value = event.target.value;
        this.setState({automobile_name: value})
    }
    handleCustomer(event) {
        const value = event.target.value;
        this.setState({customer_name: value})
    }
    handlePrice(event) {
        const value = event.target.value
        this.setState({price: value})
    }
    async componentDidMount() {
        const autoResponse = await fetch ('http://localhost:8100/api/automobiles/')
        if (autoResponse.ok){
            const data = await autoResponse.json()
            this.setState({autos: data.autos})
        }
        const custUrl = 'http://localhost:8090/api/customers/'
        const custResponse = await fetch(custUrl)
        if (custResponse.ok){
            const data = await custResponse.json()
            this.setState({customers: data.customer})
        }
        const salepUrl = 'http://localhost:8090/api/salespersons/'
        const saleResponse = await fetch (salepUrl)
        if (saleResponse.ok) {
            const data = await saleResponse.json()
            this.setState({salespeople: data.salesperson})
        }

    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sales Record</h1>
                        <form onSubmit={this.handleSubmit} id="new-sales-record-form">
                            <div className='mb-3'>
                                <select onChange={this.handleAutomobile} value={this.state.automobile_name} required id="autos" name='autos' className='form-select'>
                                    <option value="">Choose an automobile</option>
                                    {this.state.autos.map(auto => {
                                        return(
                                            <option key={auto.id} value={auto.vin}>
                                                {auto.model.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className='mb-3'>
                                <select onChange={this.handleSalesperson} value={this.state.sales_person} required id="salesperson" name='salesperson' className='form-select'>
                                    <option value="">Choose a sales person</option>
                                    {this.state.salespeople.map(sale => {
                                        return(
                                            <option key={sale.id} value={sale.id}>
                                                {sale.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className='mb-3'>
                                <select onChange={this.handleCustomer} value={this.state.customer_name} required id="customer" name='customer' className='form-select'>
                                    <option value="">Choose a customer</option>
                                    {this.state.customers.map(cust => {
                                        return(
                                            <option key={cust.id} value={cust.id}>
                                                {cust.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className='form-floating mb-3'>
                                    <input onChange={this.handlePrice} value={this.state.price} placeholder="price" required type="number" name="price" id="price" className='form-control'/>
                                    <label htmlFor="price">Price</label>
                            </div>
                            <button className='btn btn-primary'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>

        )
    }

}
export default SalesRecord;

