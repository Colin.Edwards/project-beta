import React from "react";


class ModelList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {models: []};
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok){
            const data = await response.json();
            this.setState({models: data.models})
        }
    }

    render() {
        return (
            <div className="col-8 offset-2">
                <h1 className="mt-4 mb-2">Models</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img src={model.picture_url} style={{hegiht:"100px", width:"200px"}}></img></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }   

    
} 

export default ModelList