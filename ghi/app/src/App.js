import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import TechnicianForm from './TechnicianForm';
import SalesPersonForm from './SalesPersonForm';
import SalesRecord from './SalesRecordForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ServiceForm from './ServiceForm';
import ServiceHistory from './ServiceHistory';
import ServiceList from './ServiceList';
import SalesPersonHistory from './SalesPersonHistory';
import SalesRecordList from './SalesRecordList';
import ModelForm from './ModelsForm';
import ModelList from './ModelsList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="technician/new" element={<TechnicianForm />} />
          <Route path="salespersons/new" element={<SalesPersonForm />} />
          <Route path="sales/new" element={<SalesRecord />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="manufacturer/new" element={<ManufacturerForm />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="service/new" element={<ServiceForm />} />
          <Route path="service/hisotry" element={<ServiceHistory />} />
          <Route path="salespersons/" element={<SalesPersonHistory />} />
          <Route path="sales/" element={<SalesRecordList />} />
          <Route path="service/history" element={<ServiceHistory />} />
          <Route path="service" element={<ServiceList />} />
          <Route path="models/new" element={<ModelForm />} />
          <Route path="models" element={<ModelList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
