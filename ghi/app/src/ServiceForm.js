import React from 'react';

class ServiceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: "",
            owner: "",
            date: "",
            time: "",
            reason: "",
            technicians: [],
            technician_name: "",
        }
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleOwnerChange = this.handleOwnerChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        

    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    handleOwnerChange(event) {
        const value = event.target.value;
        this.setState({ owner: value });
    }
    handleDateChange(event) {
        const value = event.target.value;
        this.setState({ date: value });
    }
    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({ time: value });
    }
    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }
    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({ technician_name: value });
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.technician = data.technician_name
        delete data.technician_name
        delete data.technicians
        const serviceUrl = 'http://localhost:8080/api/service/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                vin: "",
                owner: "",
                date: "",
                time: "",
                reason: "",
                technician: []
            };
            this.setState(cleared);
            window.location.reload();
        }
    

    }

    async componentDidMount() {
        const technicianUrl = "http://localhost:8080/api/technician/";
        const response = await fetch(technicianUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({technicians: data.technicians});
        }
    }

    render() {
        return(
            // <div className='container'>
                <div className='row'>
                    <div className='offset-3 col-6'>
                        <div className='shadow p-4 mt-4'>
                            <h1>New Service</h1>
                            <form onSubmit={this.handleSubmit} id="new-service-form">
                                <div className='form-floating mb-3'>
                                    <input onChange={this.handleVinChange} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                                    <label htmlFor="vin">Vin</label>
                                </div>
                                <div className='form-floating mb-3'>
                                    <input onChange={this.handleOwnerChange} value={this.state.owner} placeholder="Owner" required type="text" name="owner" id="owner" className="form-control"/>
                                    <label htmlFor="owner">Owner</label>
                                </div>
                                <div className='form-floating mb-3'>
                                    <input onChange={this.handleDateChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control"/>
                                    <label htmlFor="date">Date</label>
                                </div>
                                <div className='form-floating mb-3'>
                                    <input onChange={this.handleTimeChange} value={this.state.time} placeholder="Time" required type="text" name="time" id="time" className="form-control"/>
                                    <label htmlFor="time">Time</label>
                                </div>
                                <div className='form-floating mb-3'>
                                    <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                                    <label htmlFor="reason">Reason</label>
                                </div>
                                <div className='form-floating mb-3'>
                                    <select onChange={this.handleTechnicianChange} value={this.state.technician_name} placeholder="Technician" required name="technician" id="technician" className="form-select">
                                    <option value=""></option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.employee_number}>
                                                {technician.name}
                                            </option>
                                        )
                                    })}
                                    </select>
                                    <label htmlFor="technician">Technician</label>
                                </div>
                                <button className='btn btn-primary'>Submit Service</button>
                            </form>
                        </div>
                    </div>
                </div>
            // </div>
        );
    }
    
}

export default ServiceForm