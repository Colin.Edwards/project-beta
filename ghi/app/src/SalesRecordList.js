import React from 'react'

class SalesRecordList extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        salesrecord: [],
      }
      this.getSaleRecords = this.getSaleRecords.bind(this);
    }
    
    
    async getSaleRecords() {
      const url = 'http://localhost:8090/api/sales/';
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json()
          this.setState({salesrecord: data.sales_records})
        };
      } catch (e) {
        console.error(e);
      }
    }
  
    async componentDidMount() {
      this.getSaleRecords();
    }
  
    render () {
      return (
        <>
          <div className="col-8 offset-2">
            <h1>Sales Records</h1>
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Employee ID</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
          {this.state.salesrecord.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.name}</td>
                <td>{sale.salesperson.number}</td>
                <td>{sale.customer.name}</td> 
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            )
          })}
          </tbody>
        </table>
        </div>
      </>
      )
    }
  }
  export default SalesRecordList;
