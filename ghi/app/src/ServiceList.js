import React from 'react';

class ServiceList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            services: []
        }
    };
    async handleClick(id, method) {
        const clickUrl = `http://localhost:8080/api/service/${id}/`;
        var fetchConfig;
        if (method === "DELETE") {
            fetchConfig = {
                method: "DELETE",
            }
        } else {
            fetchConfig = {
                method: "PUT",
            }
        }
        const response = await fetch(clickUrl, fetchConfig);
        if (response.ok) {
            window.location.reload(false)
        } else {
            console.error(response.status);
        }
    }
    async componentDidMount() {
        const serviceUrl = 'http://localhost:8080/api/service/';
        const serviceResponse = await fetch(serviceUrl);
        if (serviceResponse.ok) {
            const data = await serviceResponse.json();
            const services = data.services.filter(service => {
                return (
                    service.status === false
                )
            })
            this.setState({services: services});
        }
    }

    render () {
        return(
            <div className='mt-4 mb-2'>
                <h1>Service List</h1>
                        <table className='table table-striped'>
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Reason</th>
                                    <th>Technician</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.services.map(service => {
                                    return(
                                        <tr key={service.id}>
                                            <td>{service.vin}</td>
                                            <td>{service.owner}</td>
                                            <td>{service.date}</td>
                                            <td>{service.time}</td>
                                            <td>{service.reason}</td>
                                            <td>{service.technician.name}</td>
                                            <td>
                                                <button onClick={() => this.handleClick(service.id, "DELETE")} className='btn btn-danger'>Cancel</button>
                                            </td>
                                            <td>
                                                <button onClick={() => this.handleClick(service.id, "PUT")} className='btn btn-success'>Finished</button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>

            </div>
        )
    }
}

export default ServiceList;