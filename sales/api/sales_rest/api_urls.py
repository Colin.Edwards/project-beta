from django.urls import path
from .api_views import (
    api_customers,
    api_customer,
    api_salespersons,
    api_salesperson,
    api_sales_records
)


urlpatterns = [
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("salespersons/", api_salespersons, name="api_salespersons"),
    path("salespersons/<int:pk>/", api_salesperson, name="api_salesperson"),
    path("sales/", api_sales_records, name="api_sales_records"),
]