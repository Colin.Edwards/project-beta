from selectors import EpollSelector
from xmlrpc.client import FastMarshaller
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Customer, SalesPerson, SalesRecord, AutomobileVO
from django.http import JsonResponse
import json

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone",
        "id"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "number",
        "id"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "salesperson",
        "price",
        "customer",
        "automobile",
        "id"
    ]
    encoders = {
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            sales_person = SalesPerson.objects.get(id=content["salesperson"])
            content["salesperson"] = sales_person
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message': "Not working"},
                status=400
            )



@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder = CustomerEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder =CustomerEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Customer not created"},
                status=400,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        customer = Customer.objects.get(id=pk)
        customer.delete()
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer_detail = Customer.objects.get(id=pk)
        return JsonResponse(
            customer_detail,
            encoder=CustomerEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_salespersons(request):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder = SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_salesperson(request, pk):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        salesperson = SalesPerson.objects.get(id=pk)
        salesperson.delete()
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        salesperson_detail = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            salesperson_detail,
            encoder=SalesPersonEncoder,
            safe=False
        )




