from django.db import models


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    number = models.CharField(max_length=200)


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)


class SalesRecord(models.Model):
    salesperson = models.ForeignKey(SalesPerson, related_name="salesperson", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name="customers", on_delete=models.CASCADE)
    automobile = models.ForeignKey(AutomobileVO, related_name="automobiles", on_delete=models.CASCADE)
    price = models.CharField(max_length=200)