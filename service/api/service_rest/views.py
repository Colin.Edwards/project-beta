from django.shortcuts import render
from .models import AutomobileVO, Technician, Service
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class ServiceListEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "owner",
        "date",
        "time",
        "reason",
        "technician",
        "is_vip",
        "status",
        "id",
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }

class ServiceDetailEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "owner",
        "date",
        "time",
        "reason",
        "technician",
        "is_vip",
        "status",
    ]

    encoder = {
        "technician": TechnicianEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_service(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse({"services": services}, encoder=ServiceListEncoder, safe=False)
    else: 
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Employee Not Found"}, status=400)
        vin_tag = content["vin"]
        if AutomobileVO.objects.filter(vin=vin_tag).exists():
            content["is_vip"] = True
        else:
            content["is_vip"] = False

        service = Service.objects.create(**content)
        return JsonResponse(service, encoder=ServiceListEncoder, safe=False)

@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_service(request, pk):
    if request.method == "GET":
        service = Service.objects.get(id=pk)
        return JsonResponse(service, encoder=ServiceListEncoder, safe=False)
    # elif request.method == "DELETE":
    #     count,_ = Service.objects.filter(id=pk).delete()
    #     return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        Service.objects.filter(id=pk).update(status=True)
        service = Service.objects.get(id=pk)
        return JsonResponse(service, encoder=ServiceListEncoder, safe=False)
    else:
        count,_ = Service.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
        

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianEncoder)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)

@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_technician(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
    elif request.method == "DELETE":
        count,_ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)

@require_http_methods(["GET"])
def api_history_service(request, pk):
    if request.method == "GET":
        services = Service.objects.filter(vin=pk)
        return JsonResponse({"services": services}, encoder=ServiceListEncoder, safe=False)
