from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=30)
    employee_number = models.SmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.name}: {self.employee_number}"

class Service(models.Model):
    vin = models.CharField(max_length=17, null=True, unique=False)
    owner = models.CharField(max_length=30)
    date = models.DateField(max_length=20, blank=True, null=True)
    time = models.TimeField(max_length=20, blank=True, null=True)
    reason = models.CharField(max_length=200)
    technician = models.ForeignKey(Technician, related_name="technician", on_delete=models.PROTECT)
    is_vip = models.BooleanField(default=False)
    status = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.owner}: {self.reason}"