# Generated by Django 4.0.3 on 2022-09-14 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='date',
            field=models.DateField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='time',
            field=models.TimeField(blank=True, max_length=20, null=True),
        ),
    ]
