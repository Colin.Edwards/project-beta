# CarCar

Team:

* Person 1 - Colin - Sales
* Person 2 - Britton - Service

## Overview

CarCar handles Service, Sales and Iventory for car dealership management system.

## Getting Started

This application is run by using Docker and React.
Steps to follow:
- Fork and clone the project from the repository: https://gitlab.com/Colin.Edwards/project-beta
- Open Docker Desktop on your machine.
- In the terminal on the main project directory use these commands to run Docker services.
  ```
  docker volume create beta-data
  docker-compose build
  docker-compose up
  ```
In the browser go to http://localhost:3000/

There are seven containers running:

Main project:

- react running on port 3000

Microservices:

- sevices api on port 8080
- sales api on port 8090
- inventory api on port 8100

Database:

- running on port 15432

and a Sales and Service poller



## Design

![](carcar-design.PNG)

Create ease-of-use dropdown functions for users to be able to get direction to all service, sales, and inventory options.


## Service microservice

The Service section of our microservice will handle all things related to appointments, technichians, records and status of said appointments. The data will be polling from the inventory microservice using the automobiles VIN# as the Value Object to tether and transfer data between services. For example, history records of the VIN's appointments will allow you to see the current and past owners, technicians, reasons for service etc. 

Another feature is the ability to manage these features, you can create new technicians and appointments. And the way Service is setup, if you were to ever delete "data" A.K.A[Fire-Employee]. All records will be protected so our microservice can withold maximum data regarding all interactions with a vehicle throughout its lifetime.

Below are the models and properties you will see in the service.


[AutomobileVO=VIN]
[Service=Appointment]

[AutomobileVO]
    * vin 

[Technician] (when form submitted - technician created)
    * name 
    * employee_number 

[Service]
    * vin = 
    * owner
    * date 
    * time 
    * technician 
    * reason 
    * vip 
    * status



## Sales microservice

The sales microservice is modeled with a Customer, Sales Person and a Sales Record which are the main entities. An Automobile value object is also used to poll the data from the Inventory microservice. This microservice handles the sales records for all vehicles sold by the company and all sales made by a specific sales person. This application can be use to register a new customer, sales person, and sales record.

**Models**

### Customer

The customer model includes customer name, address and phone number to keep track of sales information related to the sales record model.

### Sales Person

The sales person model includes name and employee number. 

### Sales Record

The sales record model includes sales person, customer, and automobile each used as the foreign key to their related names.

### AutomobileV0

The automobile is used to poll the data from the inventory api for each vehicles VIN to relate to the sales records.



